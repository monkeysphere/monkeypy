#!/usr/bin/python3

"""This program uses the PGPy library to load OpenPGP key material and
converty it into OpenSSH public keys.

It will process primary and subkeys as long as they have signing
capabilities. It does not check key validity or trust, but it will not
export expired keys. Revocation checks may fail in older PGPy
versions, which may lead to valid keys with fake revocation
certifications being excluded from the output. Keys with delegated
revocations are also excluded for now.
"""

import argparse
import logging
import sys

import pgpy
from pgpy.constants import KeyFlags
from pgpy.constants import SignatureType

from cryptography.hazmat.primitives import serialization

__description__ = 'Convert OpenPGP key material into OpenSSH keys'


# return a list of revocations for the given key
# ripped out of https://github.com/SecurityInnovation/PGPy/pull/207
def revocation_signatures(self):
    keyid, keytype = (self.fingerprint.keyid, SignatureType.KeyRevocation) if self.is_primary \
                     else (self.parent.fingerprint.keyid, SignatureType.SubkeyRevocation)
    for sig in iter(sig for sig in self._signatures
                    if all([sig.type == keytype, sig.signer == keyid, not sig.is_expired])):
        yield sig


# shortcut to extract revocation signatures from a public key
# sent as https://github.com/SecurityInnovation/PGPy/pull/227
def revocation_keys(self):
    for sig in self._signatures:
        if sig.revocation_key:
            yield sig.revocation_key


def revoked(self, key):
    for sig in revocation_signatures(key):  # needs #227
        if sig.signer not in {self.fingerprint.keyid} | set(self.subkeys):
            continue
        logging.debug('checking revocation signature %r', sig)
        verified = self.verify(key, sig)
        if verified:
            return verified
        else:
            # XXX: should not be skipped, but verification is broken:
            # https://github.com/SecurityInnovation/PGPy/issues/226
            # we'll need versioned dependencies to fix this
            logging.debug('verification of %r failed on key %r, skipping', sig, key)
            return True
    return False


def from_keyserver(fingerprint):
    pass


def check(key, primary=None):
    """check if a given key is usable

    That is:

    1. not expired

    2. not self-revoked

    And eventually:

    3. not revoked by keys in a given keyring

    4. matches a given UID

    5. is signed by one (or more) keys in a given keyring
    """
    if key.is_expired:
        logging.warning("key %r is expired, skipping", key)
        return False
    revoker = primary or key
    if revoked(revoker, key):
        logging.warning("key %r revoked by %r", key, revoker)
        return False
    try:
        revokers = revocation_keys(key)
    except NotImplementedError as e:
        # needs https://github.com/SecurityInnovation/PGPy/pull/198
        logging.exception("revocation delegation not implemented, skipping key %s", key)
        return False
    for revoker in [from_keyserver(x) for x in revokers]:
        if revoker is False:
            logging.warning("cannot fetch keys from the keyservers, revocation cannot be checked, skipped")
            return False
        if revoker.revoked(key):
            logging.warning("key %r was revoked by delegated revoker %r, skipping", key, revoker)
            return False
    return True


def key2ssh(key):
    logging.debug("outputting base64-encoded key material for key %r", key)
    material = key._key.keymaterial.__pubkey__()
    return material.public_bytes(
        encoding=serialization.Encoding.OpenSSH,
        format=serialization.PublicFormat.OpenSSH
    ).decode('utf-8')


def ssh_keys(key, flag=KeyFlags.Authentication):
    for uid in key.userids:
        main_uid = u' %s <%s>' % (uid.name, uid.email)
        break
    else:
        main_uid = ''
    if flag in key._get_key_flags():
        if check(key):
            yield key2ssh(key), key.fingerprint + main_uid
    else:
        logging.debug("primary key without flag %s, skipped: %r [%s]", flag, key, key._get_key_flags())
    for _, subkey in key.subkeys.items():
        if flag in subkey._get_key_flags():
            if check(subkey, key):
                yield key2ssh(subkey), subkey.fingerprint + main_uid
        else:
            logging.debug("subkey without flag %s, skipped: %r [%s]", flag, subkey, subkey._get_key_flags())


def main():
    parser = argparse.ArgumentParser(description=__description__,
                                     epilog=__doc__)
    default_level = 'WARNING'
    parser.add_argument('-v', '--verbose',
                        dest='loglevel', action='store_const',
                        const='INFO', default=default_level,
                        help='enable verbose messages')
    parser.add_argument('-d', '--debug',
                        dest='loglevel', action='store_const',
                        const='DEBUG', default=default_level,
                        help='enable debugging messages')
    parser.add_argument('--loglevel', choices=logging._nameToLevel.keys(),
                        default=default_level,
                        help='expliticly set logging level')
    choices = [str(f).split('.')[-1] for f in KeyFlags]
    parser.add_argument('--flag', choices=choices, default='Authentication',
                        help='select keys with the specified flag')
    parser.add_argument('path', nargs='*', default='-',
                        help='OpenPGP key material, - for stdin')
    args = parser.parse_args()

    logging.getLogger('').setLevel(args.loglevel)

    flag = getattr(KeyFlags, args.flag)
    for path in args.path:
        if path == '-':
            f = sys.stdin
        else:
            f = open(path, mode='rb')
        key, _ = pgpy.PGPKey.from_blob(f.read())
        for key, comment in ssh_keys(key, flag):
            print(key, comment)
        f.close()


if __name__ == '__main__':
    main()
