#!/usr/bin/python3

from distutils.version import LooseVersion
import os.path

import pgpy
import pytest

from ..openpgp2ssh import ssh_keys


# expected keys for my personal key
# two valid auth keys, one revoked means two good keys
#
anarcat_ssh_keys = [
        ('ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTtdA/YZOdYY35bKWKokkHkXTklnwWKbAMWbcgGaaDbPEMl+0wAm75WoBRUF/ZetwbQQ1SlNsbqymeFp2LiwbwU3xFmw7v/TAJrYJxIPEV8fjApIIao7PWzz0o8na+Ocz6w2qKWc1CJkryLT/t/JcUnPsFzlp/nYkOyrS0BqdkNwj9/hSO8zB1uaErrtc+TeiUO/Cu6oJ81LR1Rk0sRnHNBQv85W7ORVna+38LENQk05dQLuOxyf2c+TbZMJrA2d6VeZwX2hER52N23qOfyAs45f0LQOqmyk8y1BcnRykrmVlsVVgVJSBFKDRj6lMPLFrEUG0R5+p15m+W8833VpHn', 'BEE1 89C3 9F25 BD62 E9EE  089A B7F6 48FE D2DF 2587 Antoine Beaupré <anarcat@anarc.at>'),
        ('ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7CY6+aTLlk6epl1+TK6wIaHg1fageEfmKFgn+Yov+2lKFIhNRkcWznQVcyViVmC7iaZkEIei1gP9+0lrsdhewtTBjvkDNxR18aIORJsiH95FFjFIuJ0HQjrM1jOxiXhQZ0xLlnhFkxxa8j9l52HTutpYUU63e3lvY0CBuqh7QtkH3un7iT6EaqMR34yFa2ym35ag8ugMbczBwnTDJYn3qpL8gKuw3JnIp+qdSQb1sGdLcC4JN02E2/IY7iw8lzM9xVab1IgvemCJwS0C/Bt9LsmhCy9AMpaVFaAYjepgdBpSqIMa/8VcoVOrhdJWfIc7fLtt+njN1qojsPmuhsr1n', '5A23 7308 8863 DBDF 2E00  7607 604E 4B3E EE02 855A Antoine Beaupré <anarcat@anarc.at>'),
        ]

keyfiles = [
    ('anarcat-minimal.asc', anarcat_ssh_keys),
    ('anarcat-fake-minimal.asc', []),
]


@pytest.mark.parametrize('keyfile,expected', keyfiles)
@pytest.mark.xfail(LooseVersion(pgpy.__version__) < LooseVersion('0.4.4'), reason="pre 0.4.4 has bad unicode handling, see  https://github.com/SecurityInnovation/PGPy/commit/72d9fcf9b0686fbee2896e59aa41350ace591364")
def test_anarcat(keyfile, expected):
    keyfile = os.path.join(os.path.dirname(__file__), keyfile)
    key, _ = pgpy.PGPKey.from_file(keyfile)
    keys = list(ssh_keys(key, pgpy.constants.KeyFlags.Authentication))
    assert expected == keys
